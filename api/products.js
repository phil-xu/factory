var models = require('../models')
var express = require('express')
var router = express.Router()
const Op = require('sequelize').Op
router.get('/p', function (req, res, next) {
  var where = {}
  if (req.query.createdStart) {
    delete where.createdStart
    where.createdAt = {}
    where.createdAt[Op.gt] = req.query.createdStart
  }
  if (req.query.createdEnd) {
    delete where.createdEnd
    where.createdAt = where.createdAt || {}
    where.createdAt[Op.lte] = req.query.createdEnd
  }
  models.Product.findAndCountAll({
    where: where,
    include: [{
      model: models.User,
      as: 'user',
      attributes: ['name']
    }, {
      model: models.Order,
      as: 'order',
      attributes: ['orderId', 'itemId', 'name', 'submitedAt', 'quantity']
    }],
    order: [
      [models.sequelize.col('order.orderId')],
      ['createdAt']
    ],
    raw: true
  }).then(function (products) {
    res.send(products.rows)
  })
})
router.get('/', function (req, res, next) {
  var where = {}
  if (req.query.createdStart) {
    delete where.createdStart
    where.finishedAt = {}
    where.finishedAt[Op.gt] = req.query.createdStart
  }
  if (req.query.createdEnd) {
    delete where.createdEnd
    where.finishedAt = where.finishedAt || {}
    where.finishedAt[Op.lte] = req.query.createdEnd
  }
  models.Order.findAndCountAll({
    where: where
  }).then(function (products) {
    res.send(products.rows)
  })
})
router.post('/', async function (req, res, next) {
  try {
    await models.Product.create({
      userId: req.decoded.iss,
      quantity: req.body.quantity,
      orderId: req.body.orderId
    })
    res.send({})
  } catch (e) {
    console.log(e.message)
    res.send({
      error: {
        message: e.message
      }
    })
  }
})
module.exports = router
