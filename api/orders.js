var models = require('../models')
var express = require('express')
var router = express.Router()
const Op = require('sequelize').Op
router.get('/search', async function (req, res, next) {
  var where = {}
  var result = {}
  if (req.query.keywords) {
    where = {
      [Op.or]: {
        itemId: {
          [Op.like]: '%' + req.query.keywords + '%'
        },
        orderId: {
          [Op.like]: '%' + req.query.keywords + '%'
        }
      }
    }
  }
  if (req.query.role === 'worker' || req.query.role === 'null') {
    where['factory'] = req.query.factory
  }

  console.log(12, where)
  var orders = await models.Order.findAndCountAll({
    where: where,
    include: [{
      model: models.Record,
      as: 'records',
      include: [{
        model: models.User,
        as: 'user',
        attributes: ['name']
      }]
    }],
    order: [
      ['id', 'ASC'],
      [models.sequelize.col('records.id'), 'DESC']
    ]
  })
  result.orders = orders.rows
  res.send(result)
})
router.get('/', function (req, res, next) {
  var where = {}
  Object.assign(where, req.query)
  console.log(1, where)
  if (req.query.producing) { // 查询在生产
    delete where.producing
    where['status'] = {
      [Op.or]: ['喷蜡', '浇筑', '执模', '字印', '质检', '异常']
    }
  }
  if (req.query.role === 'worker' || req.query.role === 'null') {
  } else {
    delete where.factory
  }
  delete where.role
  if (req.query.finishedStart) {
    delete where.finishedStart
    where.finishedAt = {}
    where.finishedAt[Op.gt] = req.query.finishedStart
  }
  if (req.query.finishedEnd) {
    delete where.finishedEnd
    where.finishedAt = where.finishedAt || {}
    where.finishedAt[Op.lte] = req.query.finishedEnd
  }
  models.Order.findAll({
    where: where,
    include: [{
      model: models.Product,
      as: 'products',
      attributes: []
    }],
    group: 'itemId',
    attributes: {
      include: [[models.sequelize.fn('sum', models.sequelize.col('products.quantity')), 'finished']]
    },
    order: [
      ['submitedAt', 'ASC'],
      ['name', 'ASC']
    ]
  }).then(function (orders) {
    console.log(orders)
    res.send(orders)
  })
})
router.post('/:id', async function (req, res, next) {
  try {
    var order = await models.Order.findOne({
      where: {
        id: req.params.id
      },
      include: [{
        model: models.Product,
        as: 'products',
        attributes: []
      }],
      attributes: {
        include: [[models.sequelize.fn('sum', models.sequelize.col('products.quantity')), 'finished']]
      }
    })
    await order.update(req.body)
    await models.Record.create({
      userId: req.decoded.iss,
      operation: req.body,
      orderId: req.params.id
    })
    if (req.body.status === '确认') {
      await models.Product.create({
        userId: req.decoded.iss,
        quantity: order.quantity - order.get('finished', 0),
        orderId: req.params.id
      })
    }
    res.send({})
  } catch (e) {
    console.log(e.message)
    res.send({
      error: {
        message: e.message
      }
    })
  }
})
router.post('/', async function (req, res, next) {
  try {
    await models.Order.update(req.body.order, {
      where: {
        id: {
          [Op.in]: req.body.idList
        }
      }
    })
    res.send({})
  } catch (e) {
    console.log(e.message)
    res.send({
      error: {
        message: e.message
      }
    })
  }
})
module.exports = router
