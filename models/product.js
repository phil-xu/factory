module.exports = function (sequelize, DataTypes) {
  var Product = sequelize.define('Product', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    quantity: DataTypes.INTEGER
  }, {
    tableName: 'products'
  })
  Product.associate = function (models) {
    Product.belongsTo(models.User, { as: 'user', foreignKey: 'userId', targetKey: 'id' })
    Product.belongsTo(models.Order, { as: 'order', foreignKey: 'orderId', targetKey: 'id' })
  }
  return Product
}
