module.exports = function (sequelize, DataTypes) {
  var Order = sequelize.define('Order', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    orderId: DataTypes.STRING(16),
    itemId: { type: DataTypes.STRING(8) },
    serialNumber: DataTypes.STRING(8),
    quantity: DataTypes.INTEGER(4),
    name: DataTypes.STRING(128),
    image: DataTypes.STRING(128),
    note: DataTypes.STRING(256),
    orderTags: {
      type: DataTypes.STRING(1024),
      defaultValue: '[]',
      get: function () { return JSON.parse(this.getDataValue('orderTags')) },
      set: function (json) { this.setDataValue('orderTags', JSON.stringify(json)) }
    },
    options: {
      type: DataTypes.STRING(1024),
      defaultValue: '{}',
      get: function () { return JSON.parse(this.getDataValue('options')) },
      set: function (json) { this.setDataValue('options', JSON.stringify(json)) }
    },
    memo: DataTypes.STRING(256),
    status: {
      type: DataTypes.STRING(8),
      defaultValue: '喷蜡'
    },
    submitedAt: DataTypes.DATE(),
    printedAt: DataTypes.DATE(),
    castedAt: DataTypes.DATE(),
    cleanedAt: DataTypes.DATE(),
    laseredAt: DataTypes.DATE(),
    finishedAt: DataTypes.DATE(),
    errorMessage: DataTypes.STRING(256),
    repairMessage: DataTypes.STRING(256),
    urgentDate: DataTypes.DATE(),
    material: DataTypes.STRING(32),
    category: DataTypes.STRING(8),
    setting: DataTypes.BOOLEAN,
    factory: DataTypes.STRING(64)
  }, {
    tableName: 'orders'
  })
  Order.associate = function (models) {
    // Order.belongsTo(models.Shop, { as: 'shop', foreignKey: 'shopId', targetKey: 'id' })
    // Order.belongsTo(models.Product, { as: 'product', foreignKey: 'productId', targetKey: 'item_id' })
    Order.hasMany(models.Record, { as: 'records', foreignKey: 'orderId', sourceKey: 'id' })
    Order.hasMany(models.Product, { as: 'products', foreignKey: 'orderId', sourceKey: 'id' })
  }
  return Order
}
