var express = require('express')
var path = require('path')
var bodyParser = require('body-parser')
var app = express()
var compress = require('compression')
var http = require('http')
var properties = require('properties')

var models = require('./models')
app.use(compress())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
// 跨域支持
app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
  next()
})
app.use('/api', require('./api'))
app.use(express.static(path.join(__dirname, 'dist'), { maxAge: '30d' }))
var server = http.createServer(app)
models.sequelize.sync().then(function () {
  var port = process.env.PORT || 3006
  console.log('listening at :%s', port)
  server.listen(port)
})
