var jwt = require('jwt-simple')
const models = require('../models')
const secret = 'mlab'
module.exports = async function (req, res, next) {
  var token = req.headers['x-access-token']
  if (token) {
    try {
      var decoded = jwt.decode(token, secret)
      if (decoded.exp <= Date.now()) {
        res.send({
          error: {
            code: 410,
            message: 'Token已过期'
          }
        })
        return
      }
      var user = await models.User.findOne({
        where: {
          token: token
        }
      })
      if (!user) {
        res.send({
          error: {
            code: 410,
            message: 'Token无效'
          }
        })
        return
      }
      req.decoded = decoded
      console.log(decoded)
      return next()
    } catch (err) {
      // console.log(err)
      res.send({
        error: {
          code: 410,
          message: 'Token无法解密'
        }
      })
    }
  } else {
    res.send({
      error: {
        code: 410,
        message: '需要Token'
      }
    })
  }
}
